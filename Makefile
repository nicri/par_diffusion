SHELL       = /bin/sh
TARGET      = main
#----------------------------------------------------------------------
#  Compiler settings (Linux)
#----------------------------------------------------------------------
F90         = mpif90
CC          = cc
DEBUG       = 
OPT         = -O3
PARA	    =  
FFLAGS      = $(OPT) -free $(DEBUG) $(PARA) 
CFLAGS      = -O
LD          = $(F90)
LDFLAGS     = $(FFLAGS)
CPP         = /lib/cpp
DEFINE      = 
LIBS        = 

#----------------------------------------------------------------------
#  Search path for RCS files                                           
#----------------------------------------------------------------------
VPATH = ./RCS

#----------------------------------------------------------------------
#  Additional suffix rules                                             
#----------------------------------------------------------------------
.SUFFIXES : .inc .inc,v .f,v .c,v
.f,v.f :
	 co $*.f

.c,v.c :
	 co $*.c

.inc,v.inc :
	 co $*.inc

#----------------------------------------------------------------------
#  Binary directory
#----------------------------------------------------------------------
bindir      = $(HOME)/bin

#----------------------------------------------------------------------
#  Default target
#----------------------------------------------------------------------
all: $(TARGET)

#----------------------------------------------------------------------
#  Object files:                                                       
#  NOTE: you HAVE to sort the objects files such that no file will 
#  depend on files below it ! in this example, the diffuse2.f and .o
#  depends on all he module files (i named them m_*.f), and the m_init
#  depends (USE) the m_diffuse; thus m_diffuse HAS to be compiled 
#  before m_init and before diffuse2
#----------------------------------------------------------------------
OBJS =\
	global.o\
	parallel.o\
	init.o\
	save.o\
	benchmark.o\
	diffusion.o\
	main.o

#----------------------------------------------------------------------
#  Dependencies:                                                       
#  NOTE: add the dependencies here explicitly ! 
#  In that way you are sure diffuse2.f will be recompile if any of the
#  modules source files are modified.
#----------------------------------------------------------------------
main.o: main.f90 global.f90 init.f90 save.f90 diagnostic.f90 diffusion.f90 
	$(F90) $(FFLAGS)  -c main.f90
global.o: global.f90
	$(F90) $(FFLAGS)  -c global.f90
parallel.o: parallel.f90
	$(F90) $(FFLAGS)  -c parallel.f90
init.o: init.f90 global.f90 parallel.f90 
	$(F90) $(FFLAGS)  -c init.f90 
save.o: save.f90 global.f90
	$(F90) $(FFLAGS)  -c save.f90 
benchmark.o: benchmark.f90 global.f90 save.f90 parallel.f90
	$(F90) $(FFLAGS)  -c benchmark.f90 
diffusion.o: diffusion.f90 save.f90 global.f90 diagnostic.f90
	$(F90) $(FFLAGS)  -c diffusion.f90 
 
#----------------------------------------------------------------------
#  link                                                                
#----------------------------------------------------------------------
$(TARGET): $(OBJS)
	$(LD) -o $(TARGET) $(LDFLAGS) $(OBJS) $(LIBS)

#----------------------------------------------------------------------
#  Install                                                             
#----------------------------------------------------------------------
install: $(TARGET)
	(cp -f $(TARGET) $(bindir))

#----------------------------------------------------------------------
#  Run                                                                 
#----------------------------------------------------------------------
run: $(TARGET)
	$(TARGET)

#----------------------------------------------------------------------
#  Clean                                                               
#----------------------------------------------------------------------
new: cleanall main
cleanall:
	 rm -f __*.f
	 rm -f $(OBJS)
	 rm -f *.lst
	 rm -f *.mod
	 rm -f *.l
	 rm -f *.L
	 rm -f *.dat

clean:
	 rm -f __*.f
	 rm -f *.lstf
