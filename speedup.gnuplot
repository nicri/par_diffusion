reset

# wxt
#set terminal wxt size 410,250 enhanced font 'Verdana,9' 
# png
set terminal pngcairo size 800,600 
set output 'speedup.png'
# svg
#set terminal svg size 410,250 fname 'Verdana, Helvetica, Arial, sans-serif' \
#fsize '9' rounded dashed
#set output 'nice_web_plot.svg'

# define axis
# remove border on top and right and set color to gray
#set style line 11 lc rgb '#808080' lt 1
set border 11 back
set tics nomirror
# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

# color definitions
set style line 1 lc rgb '#8b1a0e' pt 6 ps 1.5 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green
set style line 3 lc rgb '#0060ad' pt 7 ps 1.5 lt 1 lw 2 # --- blue

set key bottom right

set y2tics 0,.5,2.5 textcolor rgb '#8b1a0e'
set ytics nomirror

set xlabel '# threads'
set ylabel 'Speedup'
set y2label 'Communication time [s]' tc rgb '#8b1a0e'
# set xrange [0:1]
set y2range [0:2.5]

P = 55.482674326281995 + 0.16346626495942473
plot 'speedup.dat' u 1:(P/($2+$3)) t "Diffusion Calc + Comm" w lp ls 3 axes x1y1, x t 'linear trend', \
	''	   u 1:3 t "Only Communication" w p ls 1 axes x1y2
