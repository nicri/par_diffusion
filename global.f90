MODULE globalData
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER :: Nx=4096, Ny=4096, Nx_local, Ny_local
    INTEGER :: Nt = 1000
    REAL(MK) :: D = 1.0, Lx = 1.0, Ly = 1.0
    REAL(MK), DIMENSION(:,:), ALLOCATABLE :: temp_now, temp_before
    REAL(MK) :: dX, dY, dt = 1.4901161193847656E-8 


CONTAINS
END MODULE globalData
