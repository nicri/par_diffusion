MODULE modSave
    USE globalData
    IMPLICIT NONE
CONTAINS
    SUBROUTINE saveFile(temp_now_serial,filename)
        INTEGER :: i,j
        REAL(MK), DIMENSION(Nx,Ny), INTENT(IN) :: temp_now_serial
        CHARACTER(len=*) :: filename
        ! Open the file with this name
        OPEN(10,FILE=filename)
        DO j=1,Ny
            DO i=1,Nx
                WRITE(10,'(3E12.4)') REAL(i)*dX,REAL(j)*dY,temp_now_serial(i,j)
            ENDDO
            WRITE(10,'(A)') ! Will produce a new empty line – and tell gnuplot to lift the pen
        ENDDO
        CLOSE(10)
    END subroutine
END MODULE modSave