module modParallel
    include 'mpif.h'
    INTEGER :: rank, size_Of_Cluster, ierror
    INTEGER, DIMENSION(MPI_STATUS_SIZE) :: status 
    CONTAINS
        subroutine init_parallel_mode()
            
            call MPI_INIT(ierror)
            call MPI_COMM_SIZE(MPI_COMM_WORLD, size_Of_Cluster, ierror)
            call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)
            
        end subroutine

        subroutine end_parallel_mode()
            
            call MPI_FINALIZE(ierror)
            
        end subroutine
END module modParallel