MODULE modDiagnostic
    USE globalData, ONLY : MK,temp_now,temp_before
    IMPLICIT NONE
    CONTAINS
    SUBROUTINE diagnostic(t, closeFile)
        REAL(MK), INTENT(IN) :: t
        REAL(MK) :: minimum
        LOGICAL :: firstOpening = .TRUE.
        LOGICAL, INTENT(IN), OPTIONAL :: closeFile 
        minimum = MINVAL(temp_now)
        IF(firstOpening) THEN
            firstOpening = .FALSE.
            OPEN(11,FILE='diag.dat')
        ENDIF
        WRITE(11,'(3E12.4)') t, minimum
        IF(PRESENT(closeFile)) THEN
            IF(closeFile) CLOSE(11)
        ENDIF
    END subroutine
END MODULE