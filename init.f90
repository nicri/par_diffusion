MODULE modInit
    !Module containing the subroutine to initialize the steps values and boundaries conditions
    USE globalData
    USE modParallel
    INTEGER :: i_start_local, i_end_local

CONTAINS 
    SUBROUTINE init()
        IMPLICIT NONE
        INTEGER :: i,j
        INTEGER :: info

        dX = Lx/Nx
        dY = Ly/Ny

        IF(dt> MIN(dX,dY)**2/(4*D)) THEN
            IF(rank .EQ. 0) PRINT*,"Warning! time step does not satisfy the Fourier limit!"
            IF(rank .EQ. 0) PRINT*,"Changing it to: ", MIN(dX,dY)**2/(4*D), " s"
            dt = MIN(dX,dY)**2/(4*D)
        ENDIF

        Ny_local = Ny   
        Nx_local = Nx/size_Of_Cluster
        ! +- 1 at the end is the ghost layer
        IF (rank .EQ. 0 ) THEN
            i_start_local = (rank*Nx_local+1)
            IF(size_Of_Cluster.NE.1) THEN
                i_end_local =  (rank*Nx_local) + Nx_local + 1 
            else
                i_end_local = Nx
            ENDIF
        ELSE IF(rank .NE. size_Of_Cluster-1 ) THEN
            i_start_local = (rank*Nx_local+1) - 1
            i_end_local =  (rank*Nx_local) + Nx_local + 1 
        ELSE
            !Last proc Nx_local must be overwritten 
            !to fill the remaining cells in case mod(Nx, size_of_Cluster) != 0
            Nx_local = Nx - Nx_local*(size_Of_Cluster-1)
            i_start_local = (rank*Nx_local+1) - 1
            i_end_local = Nx
        END IF
        !PRINT*, rank, i_start_local, i_end_local

        ALLOCATE(temp_before(i_start_local:i_end_local,Ny_local), stat=info)
        ALLOCATE(temp_now(i_start_local:i_end_local,Ny_local), stat=info)

        IF(info .NE. 0) PRINT*, 'Processor ', rank, ' exited with allocation error: ',info 
                
        ! Let's impose Dirichlet Boundary Condition and init to 0 the rest
        DO j=1,Ny_local
            DO i=i_start_local,i_end_local
                IF (i.EQ.1.OR.i.EQ.Nx.OR.j.EQ.1.OR.j.EQ.Ny) THEN
                    temp_before(i,j) = 1.0
                    temp_now(i,j) = 1.0
                ELSE
                    temp_before(i,j) = 0.0
                ENDIF
            ENDDO
        ENDDO
        call MPI_BARRIER(MPI_COMM_WORLD, ierror)
        !PRINT*,'Init Done'


    END subroutine
END MODULE modInit
