MODULE modBenchmark
    use globalData
    use modSave
    use modParallel
    use modInit
    IMPLICIT NONE
    contains
        SUBROUTINE benchmark_serial(temp_now_local, partial_sum_RMS)
            !COMPUTE THE PARTIAL RMS SUMS FOR THE PARALLEL PROCESS CHUNK
            REAL(MK), INTENT(OUT) :: partial_sum_RMS
            INTEGER :: i,j,t,info
            REAL(MK) :: dTdx_sq, dTdy_sq, dxsqrd, dysqrd
            REAL(MK), DIMENSION(:,:),ALLOCATABLE :: temp_now_serial, temp_before_serial
            REAL(MK), DIMENSION(:,:) :: temp_now_local

            ALLOCATE(temp_now_serial(Nx,Ny), temp_before_serial(Nx,Ny), stat=info)

            dX = Lx/Nx
            dY = Ly/Ny
            ! Let's impose Dirichlet Boundary Condition and init to 0 the rest
            DO j=1,Ny
                DO i=1,Nx
                    IF (i.EQ.1.OR.i.EQ.Nx.OR.j.EQ.1.OR.j.EQ.Ny) THEN
                        temp_before_serial(i,j) = 1.0
                        temp_now_serial(i,j) = 1.0
                    ELSE
                        temp_before_serial(i,j) = 0.0
                    ENDIF
                ENDDO
            ENDDO

            dxsqrd=1_MK/(dX*dX)
            dysqrd=1_MK/(dY*dY)
            
            DO t=1,Nt     
                DO j=2,Ny-1
                    DO i=2,Nx-1
                        dTdx_sq = (temp_before_serial(i+1,j) - 2*temp_before_serial(i,j) + temp_before_serial(i-1,j))*dxsqrd
                        dTdy_sq = (temp_before_serial(i,j+1) - 2*temp_before_serial(i,j) + temp_before_serial(i,j-1))*dysqrd
                        temp_now_serial(i,j) = temp_before_serial(i,j) + D*dt*(dTdx_sq + dTdy_sq)
                    ENDDO
                ENDDO
                DO j=2,Ny-1
                    DO i=2,Nx-1
                        temp_before_serial(i,j) = temp_now_serial(i,j)
                    ENDDO
                ENDDO
                
            ENDDO
            IF (rank.EQ.0) CALL saveFile(temp_now_serial,'diff.dat')
            partial_sum_RMS=0

            !Physical boundaries excluded for semplicity
            IF (rank.EQ.0) THEN
                DO j=1,Ny
                    DO i=i_start_local,i_end_local-1
                        partial_sum_RMS = (temp_now_serial(i,j) - temp_now_local(i,j))*(temp_now_serial(i,j) - temp_now_local(i,j))
                        !IF( mod(i,2)==0 .AND. mod(j,2)==0) PRINT*, temp_now_serial(i,j), temp_now_local(i,j)
                    ENDDO
                ENDDO
            ELSE IF (rank .EQ. size_of_Cluster -1) THEN
                DO j=1,Ny
                    DO i=i_start_local+1,i_end_local
                        partial_sum_RMS = (temp_now_serial(i,j) - temp_now_local(i,j))*(temp_now_serial(i,j) - temp_now_local(i,j))
                    ENDDO
                ENDDO
            else
                DO j=1,Ny
                    DO i=i_start_local+1,i_end_local-1
                        partial_sum_RMS = (temp_now_serial(i,j) - temp_now_local(i,j))*(temp_now_serial(i,j) - temp_now_local(i,j))
                    ENDDO
                ENDDO
            ENDIF
            DEALLOCATE(temp_now_serial,temp_before_serial)
    END subroutine
END MODULE 