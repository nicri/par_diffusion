PROGRAM main
    USE globalData
    USE modParallel
    USE modInit
    USE modDiffusion, ONLY: diffuse

    CHARACTER(24) :: date

    call init_parallel_mode()
    !IF(rank .EQ. 0) CALL fdate(date)
    !IF(rank .EQ. 0) PRINT*, 'Simulation started on ', date 
    ! Initialization of variables and boundary conditions
    CALL init()
    ! Actual calculation of diffusion 
    CALL diffuse(benchmark_flag=.TRUE.)
    !PRINT*, 'Avg CPU time ', time_metric/REAL(n)
    !IF(rank .EQ. 0) CALL fdate(date)
    !IF(rank .EQ. 0) PRINT*, 'Simulation ended on ', date 
    call end_parallel_mode()

END PROGRAM main

