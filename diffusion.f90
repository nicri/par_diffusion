MODULE modDiffusion
    !Module containing the subroutine to deal with the actual diffusion calculation and output
    USE globalData
    USE modInit
    USE modParallel
    USE modBenchmark
    IMPLICIT NONE
    INTEGER :: i,j,t,r
CONTAINS
    SUBROUTINE diffuse(benchmark_flag)
        REAL(MK) :: dTdx_sq, dTdy_sq, dxsqrd, dysqrd  
        INTEGER :: up_rank, down_rank,info,ierr
        REAL(MK) :: partial_sum_RMS, RMS
        REAL(MK), DIMENSION(:), ALLOCATABLE :: msg_to_up, msg_to_down, msg_from_up, msg_from_down
        REAL(MK) :: t1,t2,t3,t4, calc_time = 0,comm_time = 0, max_calc_time,max_comm_time 
        LOGICAL, OPTIONAL :: benchmark_flag
        !INTEGER(MK) :: count1,count2, count_rate

        ALLOCATE( msg_to_up(Ny_local), msg_to_down(Ny_local), msg_from_up(Ny_local), msg_from_down(Ny_local),stat=info)

        dxsqrd=1_MK/(dX*dX)
        dysqrd=1_MK/(dY*dY)
        
        
        DO t=1,Nt    

            t1 = MPI_WTIME()
            !Now ghost layers are treated as boundaries
            DO j=2,Ny_local-1
                DO i=i_start_local+1,i_end_local-1
                    dTdx_sq = (temp_before(i+1,j) - 2*temp_before(i,j) + temp_before(i-1,j))*dxsqrd
                    dTdy_sq = (temp_before(i,j+1) - 2*temp_before(i,j) + temp_before(i,j-1))*dysqrd
                    temp_now(i,j) = temp_before(i,j) + D*dt*(dTdx_sq + dTdy_sq)
                ENDDO
            ENDDO
            t2 = MPI_WTIME()
            !Update ghost layers
            !First/last rank has no left/right boundary to exchange with, in that case MPI_PROC_NULL
            up_rank = rank + 1;
            IF (up_rank >= size_of_Cluster) up_rank = MPI_PROC_NULL;
            down_rank = rank - 1;
            IF (down_rank < 0) down_rank = MPI_PROC_NULL;

            DO j =1,Ny_local
                msg_to_up(j) = temp_now(i_end_local-1,j)
                msg_to_down(j) = temp_now(i_start_local+1,j)
            ENDDO

            ! 0 <-> 1 exchange, 2 <-> 3 exchange, etc...
            IF ((mod(rank,2) .EQ. 0).AND.(rank .NE. 1)) THEN
                call MPI_SENDRECV(msg_to_up,Ny_local,MPI_DOUBLE_PRECISION,up_rank,0,&
                                msg_from_up,Ny_local,MPI_DOUBLE_PRECISION,up_rank,1,MPI_COMM_WORLD,status,ierror)       
            ELSE              
                call MPI_SENDRECV(msg_to_down,Ny_local,MPI_DOUBLE_PRECISION,down_rank,1,&
                                 msg_from_down,Ny_local,MPI_DOUBLE_PRECISION,down_rank,0,MPI_COMM_WORLD,status,ierror)
            END IF 

            !...then 1 <-> 2 exchange, 3 <-> 4, etc. 
            IF ((mod(rank,2) .EQ. 1) .OR.(rank .EQ. 1)) THEN
                call MPI_SENDRECV(msg_to_up,Ny_local,MPI_DOUBLE_PRECISION,up_rank,2,&
                                 msg_from_up,Ny_local,MPI_DOUBLE_PRECISION,up_rank,3,MPI_COMM_WORLD,status,ierror)
            ELSE
                call MPI_SENDRECV(msg_to_down,Ny_local,MPI_DOUBLE_PRECISION,down_rank,3,&
                                  msg_from_down,Ny_local,MPI_DOUBLE_PRECISION,down_rank,2,MPI_COMM_WORLD,status,ierror)
            END IF
            
            IF (rank .EQ. 0 ) THEN
                DO j =1,Ny_local
                    temp_now(i_end_local,j) = msg_from_up(j)
                ENDDO
            ELSE IF (rank .EQ. size_of_Cluster -1) THEN
                DO j =1,Ny_local           
                    temp_now(i_start_local,j) = msg_from_down(j)
                ENDDO
            ELSE
                DO j =1,Ny_local
                    temp_now(i_end_local,j) = msg_from_up(j)
                    temp_now(i_start_local,j) = msg_from_down(j)
                ENDDO
            ENDIF
            t3 = MPI_WTIME()
            DO j=1,Ny
                DO i=i_start_local,i_end_local
                    temp_before(i,j) = temp_now(i,j)
                ENDDO
            ENDDO
            call MPI_BARRIER(MPI_COMM_WORLD, ierror)
            t4 = MPI_WTIME()

            calc_time = calc_time + (t2-t1) + (t4-t3)
            comm_time = comm_time + (t3-t2) 
        ENDDO 

        ! IF (rank.EQ.0) THEN
        !     OPEN(10,FILE='diff_par0.dat')
        !     DO j=1,Ny
        !         DO i=i_start_local,i_end_local
        !             WRITE(10,'(3E12.4)') REAL(i)*dX,REAL(j)*dY,temp_now(i,j)
        !         ENDDO
        !     WRITE(10,'(A)') ! Will produce a new empty line – and tell gnuplot to lift the pen
        !     ENDDO
        ! CLOSE(10)
        ! ENDIF
        ! IF (rank.EQ.1) THEN
        !     OPEN(10,FILE='diff_par1.dat')
        !     DO j=1,Ny
        !         DO i=i_start_local,i_end_local
        !             WRITE(10,'(3E12.4)') REAL(i)*dX,REAL(j)*dY,temp_now(i,j)
        !         ENDDO
        !     WRITE(10,'(A)') ! Will produce a new empty line – and tell gnuplot to lift the pen
        !     ENDDO
        ! CLOSE(10)
        ! ENDIF
        ! IF (rank.EQ.2) THEN
        !     OPEN(10,FILE='diff_par2.dat')
        !     DO j=1,Ny
        !         DO i=i_start_local,i_end_local
        !             WRITE(10,'(3E12.4)') REAL(i)*dX,REAL(j)*dY,temp_now(i,j)
        !         ENDDO
        !     WRITE(10,'(A)') ! Will produce a new empty line – and tell gnuplot to lift the pen
        !     ENDDO
        ! CLOSE(10)
        ! ENDIF
        ! IF (rank.EQ.3) THEN
        !     OPEN(10,FILE='diff_par3.dat')
        !     DO j=1,Ny
        !         DO i=i_start_local,i_end_local
        !             WRITE(10,'(3E12.4)') REAL(i)*dX,REAL(j)*dY,temp_now(i,j)
        !         ENDDO
        !     WRITE(10,'(A)') ! Will produce a new empty line – and tell gnuplot to lift the pen
        !     ENDDO
        ! CLOSE(10)
        ! ENDIF
        IF (size_of_Cluster .NE. 1 .AND. benchmark_flag) THEN
            CALL benchmark_serial(temp_now,partial_sum_RMS)
            CALL  MPI_ALLREDUCE( partial_sum_RMS, RMS, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr )
        
            RMS = sqrt(1.0/(Nx*Ny)*RMS)

            IF(rank.EQ. 0) PRINT*, 'RMS: ', RMS
        ENDIF
        !Let's take the max for each time
        call MPI_ALLREDUCE(calc_time, max_calc_time, 1, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierr )
        call MPI_ALLREDUCE(comm_time, max_comm_time, 1, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierr )
        IF(rank.EQ. 0) PRINT*,'Diffusion calculation done in wall time: ',max_calc_time,'s'
        IF(rank.EQ. 0) PRINT*,'Communications done in wall time: ',max_comm_time,'s'

        DEALLOCATE(temp_now,temp_before,msg_to_up, msg_to_down, msg_from_up, msg_from_down)
        call MPI_BARRIER(MPI_COMM_WORLD, ierror)

    END subroutine

END MODULE modDiffusion